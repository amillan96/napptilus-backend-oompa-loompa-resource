# Napptilus backend "Oompa-Loompa" | Final version

This application is a technical challenge proposed by Napptilus Tech Labs. The main goal of this challenge is to replicate the backend end-points behavior from scratch.

## Features:
- Social OAuth system  with Google.
- Middleware secured endpoints.
- API endpoints.
- Profile settings:
    - Development
    - Production
- Deployed on Heroku.

## Tech Stack
- Database: MongoDB with mLab.
- Back-end server: NodeJS
- NPM modules:
    - Passport: OAuth management.
    - Mongoose: MongoDB integration.
    - Mongoose-pagination: Pagination.
    - Express: HTTP Requests
    - Mocha, Chai, Superagent : Testing
- Deployment: Heroku

## API Endpoints documentation

https://documenter.getpostman.com/view/4320152/napptilus-oompa-loompa-endpoints/RW1hhFdP

## Heroku link

https://salty-river-72963.herokuapp.com



