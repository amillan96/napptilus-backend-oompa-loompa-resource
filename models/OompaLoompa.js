const mongoose = require('mongoose');
const { Schema } = mongoose;
const mongoosePaginate = require('mongoose-paginate');

const favorite = new Schema({
    color: String,
    food: String,
    random_string: String,
    song: String
});

const oompaLoompaSchema = new Schema({
    first_name: String,
    last_name: String,
    description: String,
    image: String,
    profession: String,
    quota: String,
    height: Number,
    country: String,
    age: Number,
    favorite: {
        color: String,
        food: String,
        song: String,
        random_string: String,
    },
    gender: String,
    email: String
});

oompaLoompaSchema.plugin(mongoosePaginate);
mongoose.model('oompaLoompas', oompaLoompaSchema);