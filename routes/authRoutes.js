const passport = require('passport');

module.exports = (app) => {

    /**
     * GET: Google OAuth endpoint.
     */
    app.get('/napptilus/auth/google', passport.authenticate('google', {
        scope: ['profile', 'email']
    }));

    /**
     * GET: Google OAuth callback.
     */
    app.get('/napptilus/auth/google/callback', passport.authenticate('google'), (err, res) => {
        if (err) {
            res.status(401).json({
                ok: false,
                message: 'Authentication failed',
                err: err
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Authentication successfull'
        })
    });

    /**
     * GET: Logout request.
     */
    app.get('/napptilus/logout', (req, res) => {
        req.logout();
        res.status(200).send(req.user);
    });

    /**
     * GET: Current logged user.
     */
    app.get('/napptilus/current-user', (req, res) => {
        if (!req.user) {
            res.status(302).json({
                ok: false,
                message: 'No current user logged in!'
            })
        }
        res.status(200).send(req.user);
    });
}