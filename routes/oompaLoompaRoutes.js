const mongoose = require('mongoose');
const OompaLoompa = mongoose.model('oompaLoompas');
const requireLogin = require('../middlewares/requireLogin');

module.exports = (app) => {

    /**
     * @Secured
     * POST: Creates 100 Oompa-Loompas.
     */
    app.post('/napptilus/oompa-loompas', requireLogin, (req, res) => {
        let oompaLoompas = [];
        for (let i = 0; i < 100; i++) {
            rnd = Math.floor(Math.random() * 8) + 0;
            oompaLoompas.push(new OompaLoompa({
                first_name: `firstName${i}`,
                last_name: `lastName${i}`,
                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                image: `https://salty-river-72963.herokuapp.com/napptilus/level-test/${rnd}.jpg`,
                profession: `profession${i}`,
                quota: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna.",
                height: i,
                country: `country${i}`,
                age: i,
                favorite: {
                    color: `color${i}`,
                    food: `food${i}`,
                    random_String: Math.random().toString(36).substring(7),
                    song: "Oompa Loompas:\nOompa Loompa doompadee doo\nI've got another puzzle for you\nOompa Loompa doompadah dee\nIf you are wise you'll listen to me\nWhat do you get from a glut of TV?\nA pain in the neck and an IQ of three\nWhy don't you try simply reading a book?\nOr could you just not bear to look?\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no commercials\nOompa Loompa Doompadee Dah\nIf you're like reading you will go far\nYou will live in happiness too\nLike the Oompa\nOompa Loompa doompadee do\nOompa Loompas:\nOompa Loompa doompadee doo\nI've got another puzzle for you\nOompa Loompa doompadah dee\nIf you are wise you'll listen to me\nWhat do you get from a glut of TV?\nA pain in the neck and an IQ of three\nWhy don't you try simply reading a book?\nOr could you just not bear to look?\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no commercials\nOompa Loompa Doompadee Dah\nIf you're like reading you will go far\nYou will live in happiness too\nLike the Oompa\nOompa Loompa doompadee do\nOompa Loompas:\nOompa Loompa doompadee doo\nI've got another puzzle for you\nOompa Loompa doompadah dee\nIf you are wise you'll listen to me\nWhat do you get from a glut of TV?\nA pain in the neck and an IQ of three\nWhy don't you try simply reading a book?\nOr could you just not bear to look?\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no commercials\nOompa Loompa Doompadee Dah\nIf you're like reading you will go far\nYou will live in happiness too\nLike the Oompa\nOompa Loompa doompadee do\nOompa Loompas:\nOompa Loompa doompadee doo\nI've got another puzzle for you\nOompa Loompa doompadah dee\nIf you are wise you'll listen to me\nWhat do you get from a glut of TV?\nA pain in the neck and an IQ of three\nWhy don't you try simply reading a book?\nOr could you just not bear to look?\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no commercials\nOompa Loompa Doompadee Dah\nIf you're like reading you will go far\nYou will live in happiness too\nLike the Oompa\nOompa Loompa doompadee do\nOompa Loompas:\nOompa Loompa doompadee doo\nI've got another puzzle for you\nOompa Loompa doompadah dee\nIf you are wise you'll listen to me\nWhat do you get from a glut of TV?\nA pain in the neck and an IQ of three\nWhy don't you try simply reading a book?\nOr could you just not bear to look?\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no commercials\nOompa Loompa Doompadee Dah\nIf you're like reading you will go far\nYou will live in happiness too\nLike the Oompa\nOompa Loompa doompadee do\nOompa Loompas:\nOompa Loompa doompadee doo\nI've got another puzzle for you\nOompa Loompa doompadah dee\nIf you are wise you'll listen to me\nWhat do you get from a glut of TV?\nA pain in the neck and an IQ of three\nWhy don't you try simply reading a book?\nOr could you just not bear to look?\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no\nYou'll get no commercials\nOompa Loompa Doompadee Dah\nIf you're like reading you will go far\nYou will live in happiness too\nLike the Oompa\nOompa Loompa doompadee do"
                },
                gender: i % 2 == 0 ? 'M' : 'F',
                email: `email${i}@domain.com`
            }));
        };

        OompaLoompa.insertMany(oompaLoompas, (err, oompaLoompas) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: "Error creating oompa loompas",
                    err: err
                })
            }
            res.status(201).json({
                ok: true,
                oompaLoompas: oompaLoompas
            })
        })
    })

    /**
     * GET: Fetch all Oompa-Loompas paginated by 20.
     */
    app.get('/napptilus/oompa-loompas', (req, res, next) => {

        var page = Number(req.query.page) || 1;
        var limit = Number(req.query.limit) || 20;
        var query = {};
        var options = {
            select: 'first_name last_name favorite gender image profession email age country height id',
            page: page,
            limit: limit
        };

        OompaLoompa.paginate(query, options, (err, response) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: "Error fetching oompa loompas",
                    err: err
                });
            }
            if (response.total > page)
                res.status(200).json({
                    current: response.page,
                    total: response.limit,
                    results: response.docs
                })
        });
    });

    /**
     * GET: Fetch one Oompa-Loompa by his ID.
     */
    app.get('/napptilus/oompa-loompas/:id', (req, res) => {
        var id = req.params.id;
        OompaLoompa.findById(id)
            .select('-_id -__v')
            .exec((err, oompaLoompa) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        message: 'Error fetching Oompa Loompa',
                        err: err
                    })
                }
                if (!oompaLoompa) {
                    return res.status(400).json({
                        ok: false,
                        message: `Oompa Loompa with id: ${id} doesn't exist`,
                        err: err
                    })
                }
                res.status(200).json(oompaLoompa);
            })
    });
}