const fs = require('fs')

module.exports = (app) => {

    /**
     * GET: Returns an image.
     */
    app.get('/napptilus/level-test/imgs/:img', (req, res, next) => {
        const img = req.params.img;
        const path = __dirname + '/../assets/' + img;

        fs.exists(path, exists => {
            if (!exists) {
                res.status(404).json({
                    ok: false,
                    message: 'Not found.'
                })
            } else {
                res.sendFile(img, { root: __dirname + '/../assets/' });
            }
        })
    });
}