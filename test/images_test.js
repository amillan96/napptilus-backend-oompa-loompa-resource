const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const api = supertest('http://localhost:5000')

describe('GET | Image', function(done) {

    it('should return a 200 response',
        function(done) {
            api.get('/napptilus/level-test/imgs/0.jpg')
                .set('Accept', 'application/json')
                .expect(200, done);
        })
});

describe('GET | Image does not exist', function(done) {

    it('should return a 404 response',
        function(done) {
            api.get('/napptilus/level-test/imgs/xxxxxxxxxxxxxxxxxxxxxxxx.jpg')
                .set('Accept', 'application/json')
                .expect(404, done);
        })
});