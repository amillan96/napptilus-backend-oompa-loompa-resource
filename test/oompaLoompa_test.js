const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const api = supertest('http://localhost:5000')

describe('GET | Oompa Loompas', function(done) {

    it('should return a 200 response',
        function(done) {
            api.get('/napptilus/oompa-loompas')
                .set('Accept', 'application/json')
                .expect(200, done);
        })
});

describe('GET | Oompa Loompa', function(done) {

    it('should return a 200 response',
        function(done) {
            api.get('/napptilus/oompa-loompas/5af334255a7d380ce009021a')
                .set('Accept', 'application/json')
                .expect(200, done);
        })
});

describe('GET | Oompa Loompa does not exist', function(done) {

    it('should return a 500 response',
        function(done) {
            api.get('/napptilus/oompa-loompas/xxxxxxxxxxxxxxxxxxxxxxxx')
                .set('Accept', 'application/json')
                .expect(500, done);
        })
});