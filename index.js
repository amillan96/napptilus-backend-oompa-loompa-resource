const express = require('express');
const authRoutes = require('./routes/authRoutes');
const passport = require('passport');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const keys = require('./config/keys');
require('./models/User');
require('./models/OompaLoompa');
const oompaLoompaRoutes = require('./routes/oompaLoompaRoutes');
const imagesRoutes = require('./routes/imagesRoutes');
require('./services/passport');

mongoose.connect(keys.mongoURI);

const app = express();

app.use(
    cookieSession({
        maxAge: 30 * 24 * 60 * 60 * 1000,
        keys: [keys.cookieKey]
    })
);

app.use(passport.initialize());
app.use(passport.session());

authRoutes(app);
oompaLoompaRoutes(app);
imagesRoutes(app);

const PORT = process.env.PORT || 5000
app.listen(PORT);